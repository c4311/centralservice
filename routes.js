const express = require('express');
const controller = require('./controllers/controller')
const middleware = require('./Middleware/authenticator')

const routes = express();

routes.get('/gameRank', middleware.verifyUser, controller.getGameAcRank);
routes.get('/gameName', middleware.verifyUser, controller.getGameAcName);
routes.get('/gamePlatform', middleware.verifyUser, controller.getGameAcPlatform);
routes.get('/gameYear', middleware.verifyUser, controller.getGameAcYear);
routes.get('/gameGenre', middleware.verifyUser, controller.getGameAcGenre);
routes.get('/gameTopFiveSaled', middleware.verifyUser, controller.getGameAcYearAndPlatform);
routes.get('/gameSaledMoreInEurope', middleware.verifyUser, controller.getGameSaledMoreInEurope);

module.exports = routes;