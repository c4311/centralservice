const pool = require('pg')
require('dotenv').config()

var poolins = new pool.Pool({
    user: process.env.DATABASE_USER,
    host: process.env.DATABASE_HOST,
    database: process.env.DATABASE,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT,
    ssl: false
})

const model = {}

model.gameAccordingToRank = async function (rank) {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "Rank"=$1', [rank]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}

model.gameAccordingToName = async function (name) {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "Name" like $1', ['%' + name + '%']);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}

model.gameAccordingToPlatform = async function (platform, N) {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "Platform"=$1 order by "Rank" limit $2', [platform, N]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}

model.gameAccordingToYear = async function (year, N) {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "Year"=$1 order by "Rank" limit $2', [year, N]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}

model.gameAccordingToGenre = async function (genre, N) {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "Genre"=$1 order by "Rank" limit $2', [genre, N]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}

model.gameAccordingToYearAndPlatform = async function (year, platform) {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "Year"=$1 and "Platform"=$2 order by "Global_Sales" desc limit 5', [year, platform]);
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}

model.gameSaledMoreInEurope = async function () {
    let result = {}
    try {
        let tmp = await poolins.query('select * from vgsales where "EU_Sales" > "NA_Sales"');
        result.success = true
        result.rows = tmp.rows
    } catch (error) {
        result.success = false
        result.error = error
    }
    return result
}


module.exports = model