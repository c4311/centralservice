const {json} = require('body-parser')
const model = require('../models/model')
const msgs = require('./strings')

const controller = {}

controller.getGameAcRank = async (req, res) => {
    let result = {}
    rank = req.query['rank'];
    try {
        tmp = await model.gameAccordingToRank(rank)
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.getGameAcName = async (req, res) => {
    let result = {}
    name1 = req.query['name'];
    try {
        tmp = await model.gameAccordingToName(name1);
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.getGameAcPlatform = async (req, res) => {
    let result = {}
    platform = req.query['platform'];
    N = req.query['N'];
    try {
        tmp = await model.gameAccordingToPlatform(platform, N);
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.getGameAcYear = async (req, res) => {
    let result = {}
    year = req.query['year'];
    N = req.query['N'];
    try {
        tmp = await model.gameAccordingToYear(year, N);
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.getGameAcGenre = async (req, res) => {
    let result = {}
    genre = req.query['genre'];
    N = req.query['N'];
    try {
        tmp = await model.gameAccordingToGenre(genre, N);
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.getGameAcYearAndPlatform = async (req, res) => {
    let result = {}
    year = req.query['year'];
    platform = req.query['platform'];
    try {
        tmp = await model.gameAccordingToYearAndPlatform(year, platform);
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}

controller.getGameSaledMoreInEurope = async (req, res) => {
    let result = {}
    try {
        tmp = await model.gameSaledMoreInEurope();
        if (tmp.success && tmp.rows) {
            result.data = tmp.rows;
            result.msg = msgs.msg_successful_en;
            res.status(msgs.rc_Successful);
        } else {
            result.msg = msgs.msg_errorDB_en;
            res.status(msgs.rc_InternalServerError);
        }
    } catch (error) {
        result.msg = msgs.msg_errorCONT_en
        res.status(msgs.rc_InternalServerError);
    }
    res.send(JSON.stringify(result))
}


module.exports = controller