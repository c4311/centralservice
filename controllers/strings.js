module.exports = {
    msg_successful_en: 'ok',
    msg_successful_pr: 'درخواست با موفقیت انجام شد',
    msg_errorDB_en: 'database error',
    msg_errorDB_pr: 'خطا در پایگاه داده',
    msg_errorCONT_en: 'controller error',
    msg_errorCONT_pr: 'خطا در کنترلر',
    rc_InternalServerError: 500,
    rc_Successful: 200,
    rc_Unautorized: 403,
    rc_NotFound: 404
}